const Article = require('../models/article')

exports.renderArticles = (req, res) => {
    res.redirect('/');
}

exports.renderArticlesNew = (req, res) => {
    res.render('new', {article: new Article()});
}
exports.renderIndex = async (req, res) => {
    const articles = await Article.find().sort({createdAt: 'desc'});
    res.render('index', { articles: articles });
}   

exports.createNewArticle = async (req, res) => {
    let article = new Article ({
        title: req.body.title,
        description: req.body.description,
        markdown: req.body.markdown
    })
    try{
        article = await article.save();
        res.redirect(`/articles/${article.slug}`)
    } catch(e) {
        console.log(e);
        res.render('new', {article: article})
    }
}

exports.renderArticleSingle = async (req, res) => {
    const article = await Article.findOne({slug: req.params.slug});
    
    if(article == null) {
        res.redirect('/')
    }
    res.render('single', {article: article})
}

exports.editArticle = async (req, res) => {
    const article = await Article.findById(req.params.id)
    res.render('edit', {article: article})
}

exports.updateArticle = async (req, res) => {
    req.article = await Article.findById(req.params.id);
    let article = req.article;
    article.title = req.body.title;
    article.description = req.body.description;
    article.markdown = req.body.markdown;
    
    
    try{
        article = await article.save();
        res.redirect(`/articles/${article.slug}`)
    } catch(e) {
        console.log(e);
        res.render(`articles/edit/${article.id}`, {article: article})
    }
}

exports.deleteArticle = async (req, res) => {
    await Article.findByIdAndDelete(req.params.id);
    res.redirect('/');
}