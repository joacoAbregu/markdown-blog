var express = require('express');
var router = express.Router();
const controller = require ('../controllers/controllers');

/* GET home page. */
router.get('/', controller.renderIndex);

module.exports = router;
