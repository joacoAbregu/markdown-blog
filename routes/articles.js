var express = require('express');
var router = express.Router();
const controller = require ('../controllers/controllers');

/* GET users listing. */
router.get('/', controller.renderArticles);
router.get('/new', controller.renderArticlesNew);
router.get('/:slug', controller.renderArticleSingle)
router.post('/', controller.createNewArticle);
router.get('/edit/:id', controller.editArticle);
router.post('/edit/:id', controller.updateArticle);
router.post('/:id', controller.deleteArticle);

module.exports = router;
